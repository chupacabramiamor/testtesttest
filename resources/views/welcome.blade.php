<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel Test</title>

        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body>
        <div class="d-flex justify-content-center">
            <div class="card">
                <form id="feedback-form">
                    <div class="card-header">Feedback Form</div>
                    <div class="class-body" id="feedback-successful" style="display: none">
                        <div class="text-success">Your message sent!</div>
                    </div>
                    <div class="card-body" id="feedback-form-fields">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address <span class="text-danger">*</span></label>
                                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="input-firstname">Firstname <span class="text-danger">*</span></label>
                                    <input name="firstname" class="form-control" id="input-firstname" placeholder="Your Firtname">
                                    <small class="form-text text-muted"></small>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="input-lastname">Lastname <span class="text-danger">*</span></label>
                                    <input name="lastname" class="form-control" id="input-lastname" placeholder="Your Lastname">
                                    <small class="form-text text-muted"></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-primary">Send Notification</button>
                    </div>
                </form>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

        <script>
            $('#feedback-form').on('submit', function(e) {
                e.preventDefault();

                var params = new FormData(this);

                axios.post('/rest/feedback', new FormData(this)).then(function(response) {
                    $(this).find('#feedback-form-fields').hide();
                    $(this).find('.card-footer').hide();
                    $('#feedback-successful').show();
                }.bind(this), function(error) {
                    if (error.status == 422) {
                        alert('Please fill the all required fields or verify it is corrected');
                        return;
                    }

                    alert(error.response.data.message || 'Unexpected error occured');
                });
            });
        </script>
    </body>
</html>
