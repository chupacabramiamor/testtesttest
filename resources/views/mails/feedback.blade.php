<h1>Hi, Admin!</h1>
<h3>You have received a new feedback from your form!</h3>
<hr>
<h3>Customer Details:</h3>
<ul>
    <li><b>EMAIL:</b> {{ $feedbackMessage->email }}</li>
    <li><b>FIRSTNAME:</b> {{ $feedbackMessage->firstname }}</li>
    <li><b>LASTNAME:</b> {{ $feedbackMessage->lastname }}</li>
</ul>