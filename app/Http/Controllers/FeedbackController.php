<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Http\Resources\SuccessResource;
use App\Services\AppService;

class FeedbackController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(FeedbackRequest $request, AppService $appSvc)
    {
        $appSvc->sendFeedback(
            $request->input('email'),
            $request->input('firstname'),
            $request->input('lastname')
        );

        return new SuccessResource();
    }
}
