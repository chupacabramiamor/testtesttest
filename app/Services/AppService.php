<?php

namespace App\Services;

use App\Jobs\FeedbackMailingJob;
use App\Models\FeedbackMessage;

/**
 *  AppService
 */
class AppService
{
    /**
     * Sending Feedback process
     *
     * @param  string $email     Customer Email
     * @param  string $firstname Customer Firstname
     * @param  string $lastname  Customer Lastname
     * @return void
     */
    public function sendFeedback(string $email, string $firstname, string $lastname): void
    {
        $feedbackMessage = FeedbackMessage::create(compact('email', 'firstname', 'lastname'));

        FeedbackMailingJob::dispatch($feedbackMessage);
    }
}