<?php

namespace App\Jobs;

use App\Mail\FeedbackMail;
use App\Models\FeedbackMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class FeedbackMailingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $feedbackMessage;

    /**
     * Create a new job instance.
     *
     * @param FeedbackMessage $feedbackMessage
     * @return void
     */
    public function __construct(FeedbackMessage $feedbackMessage)
    {
        $this->feedbackMessage = $feedbackMessage;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to(config('tt.administrator.email'))
            ->send(new FeedbackMail($this->feedbackMessage));
    }
}
