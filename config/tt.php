<?php

return [
    'administrator' => [
        'email' => env('ADMIN_EMAIL', 'admin@example.com')
    ]
];