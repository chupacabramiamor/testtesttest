<?php

namespace Tests\Feature;

use App\Models\FeedbackMessage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FeedbackTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSendsFeedback()
    {
        $params = [
            'email' => $this->faker->safeEmail,
            'firstname' => $this->faker->firstName(),
            'lastname' => $this->faker->lastName
        ];

        $crawler = $this->json('POST', '/rest/feedback', $params);

        $crawler->assertok();

        $crawler->assertJson([
            'message' => 'ok'
        ]);

        $feedbackMessage = FeedbackMessage::whereEmail($params['email'])->orderBy('id', 'desc')->first();

        $this->assertEquals($params['email'], $feedbackMessage->email);
        $this->assertEquals($params['firstname'], $feedbackMessage->firstname);
        $this->assertEquals($params['lastname'], $feedbackMessage->lastname);
    }
}
